package nl.imre.tbl;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import nl.han.ica.oopg.engine.GameEngine;
import nl.han.ica.oopg.objects.Sprite;
import nl.han.ica.oopg.tile.TileMap;
import nl.han.ica.oopg.tile.TileType;
import nl.han.ica.oopg.view.View;
import nl.imre.tbl.entities.Enemy;
import nl.imre.tbl.entities.Player;
import nl.imre.tbl.tiles.DoorClosedTile;
import nl.imre.tbl.tiles.DoorOpenTile;
import nl.imre.tbl.tiles.FloorTile;
import nl.imre.tbl.tiles.TrapTile;
import nl.imre.tbl.tiles.WallTile;

/**
 * @author imreb
 *
 */
@SuppressWarnings("serial")
public class World extends GameEngine implements KeyListener {
	public static void main(String[] args) {
		World world = new World();
		world.runSketch();
	}
	
	private Player player;
	private Enemy enemy;

	// KEY LISTENER
	@Override
	public void addNotify() {
		super.addNotify();
		addKeyListener(this);
	}

	private void initializeTileMap() {

		TileType<WallTile> wall = new TileType<>(WallTile.class, new Sprite(Sprites.WALL.getUrl()));
		TileType<WallTile> wallGoo = new TileType<>(WallTile.class, new Sprite(Sprites.WALLGOO.getUrl()));
		TileType<WallTile> wallHole1 = new TileType<>(WallTile.class, new Sprite(Sprites.WALLHOLE1.getUrl()));
		TileType<WallTile> wallHole2 = new TileType<>(WallTile.class, new Sprite(Sprites.WALL.getUrl()));
		TileType<WallTile> wallBannerYellow = new TileType<>(WallTile.class, new Sprite(Sprites.WALLBANNERYELLOW.getUrl()));
		TileType<WallTile> wallBannerRed = new TileType<>(WallTile.class, new Sprite(Sprites.WALLBANNERRED.getUrl()));
		TileType<WallTile> wallBannerBlue = new TileType<>(WallTile.class, new Sprite(Sprites.WALLBANNERBLUE.getUrl()));
		TileType<WallTile> wallBannerGreen = new TileType<>(WallTile.class, new Sprite(Sprites.WALLBANNERGREEN.getUrl()));

		TileType<FloorTile> floor1 = new TileType<>(FloorTile.class, new Sprite(Sprites.FLOOR1.getUrl()));
		TileType<FloorTile> floor2 = new TileType<>(FloorTile.class, new Sprite(Sprites.FLOOR2.getUrl()));
		TileType<FloorTile> floor3 = new TileType<>(FloorTile.class, new Sprite(Sprites.FLOOR3.getUrl()));
		TileType<FloorTile> floor4 = new TileType<>(FloorTile.class, new Sprite(Sprites.FLOOR4.getUrl()));
		TileType<FloorTile> floor5 = new TileType<>(FloorTile.class, new Sprite(Sprites.FLOOR5.getUrl()));
		TileType<FloorTile> floor6 = new TileType<>(FloorTile.class, new Sprite(Sprites.FLOOR6.getUrl()));
		TileType<FloorTile> floor7 = new TileType<>(FloorTile.class, new Sprite(Sprites.FLOOR7.getUrl()));
		TileType<FloorTile> floor8 = new TileType<>(FloorTile.class, new Sprite(Sprites.FLOOR8.getUrl()));
		TileType<TrapTile> trap = new TileType<>(TrapTile.class, new Sprite(Sprites.TRAP.getUrl()));

		
		TileType<DoorClosedTile> doorClosed = new TileType<>(DoorClosedTile.class, new Sprite(Sprites.DOORCLOSED.getUrl()));
		TileType<DoorOpenTile> doorOpen = new TileType<>(DoorOpenTile.class,  new Sprite(Sprites.DOOROPEN.getUrl()));
		
		@SuppressWarnings("rawtypes")
		TileType[] tileTypes = { floor1, floor2, floor3, floor4, floor5, floor6, floor7, floor8, trap, wallGoo, wallHole1, wallHole2, wall, wallBannerBlue, wallBannerGreen, wallBannerRed, wallBannerYellow, doorClosed, doorOpen };
		int tileSize = 64;
		int tilesMap[][] = Rooms.ENDROOM.getMap();

		tileMap = new TileMap(tileSize, tileTypes, tilesMap);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		super.keyPressed(e);
		int keyCode = e.getKeyCode();
		if (keyCode == KeyEvent.VK_A) {
			player.setLeft(true);
		}
		if (keyCode == KeyEvent.VK_D) {
			player.setRight(true);
		}
		if (keyCode == KeyEvent.VK_W) {
			player.setUp(true);
		}
		if (keyCode == KeyEvent.VK_S) {
			player.setDown(true);
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		super.keyReleased(e);
		int keyCode = e.getKeyCode();
		if (keyCode == KeyEvent.VK_A) {
			player.setLeft(false);
		}
		if (keyCode == KeyEvent.VK_D) {
			player.setRight(false);
		}
		if (keyCode == KeyEvent.VK_W) {
			player.setUp(false);
		}
		if (keyCode == KeyEvent.VK_S) {
			player.setDown(false);
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		super.keyTyped(e);
	}

	@Override
	public void setupGame() {
		int worldWidth = 1856;
		int worldHeight = 960;
		initializeTileMap();
		
		player = new Player(this);
		enemy = new Enemy(this);
		
		addGameObject(player, 100, 100);
		addGameObject(enemy, 500, 500);
		
		View view = new View(worldWidth, worldHeight);
		setView(view);
		size(worldWidth, worldHeight);
	}

	@Override
	public void update() {
		player.update();
		enemy.update();
	}
}
