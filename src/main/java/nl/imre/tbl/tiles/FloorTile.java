package nl.imre.tbl.tiles;

import nl.han.ica.oopg.objects.Sprite;
import nl.han.ica.oopg.tile.Tile;

/**
 * @author imreb
 *
 */
public class FloorTile extends Tile {
	/**
	 * @param sprite The image which will be drawn whenever the draw method of the
	 *               Tile is called.
	 */
	public FloorTile(Sprite sprite) {
		super(sprite);
	}
}
