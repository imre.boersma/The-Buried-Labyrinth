package nl.imre.tbl.tiles;

import nl.han.ica.oopg.objects.Sprite;
import nl.han.ica.oopg.tile.Tile;

/**
 * Created by ralphniels on 08-06-15.
 */
public class WallTile extends Tile {
	/**
	 * @param sprite The image which will be drawn whenever the draw method of the
	 *               Tile is called.
	 */
	public WallTile(Sprite sprite) {
		super(sprite);
	}
}
