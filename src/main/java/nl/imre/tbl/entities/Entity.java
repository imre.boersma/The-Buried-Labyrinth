package nl.imre.tbl.entities;

import java.util.List;

import nl.han.ica.oopg.collision.CollidedTile;
import nl.han.ica.oopg.collision.CollisionSide;
import nl.han.ica.oopg.collision.ICollidableWithGameObjects;
import nl.han.ica.oopg.collision.ICollidableWithTiles;
import nl.han.ica.oopg.exceptions.TileNotFoundException;
import nl.han.ica.oopg.objects.GameObject;
import nl.han.ica.oopg.objects.Sprite;
import nl.han.ica.oopg.objects.SpriteObject;
import nl.imre.tbl.World;
import nl.imre.tbl.tiles.WallTile;
import processing.core.PVector;

public class Entity extends SpriteObject implements ICollidableWithGameObjects, ICollidableWithTiles {
	
	private final World world;
	
	private int x;
	private int y;
	private float dx;
	private float dy;
	private float speed;
	
	public Entity(Sprite sprite) {
		super(sprite);
		this.world = new World();
		this.x = (int)Math.floor(width);
		this.y = (int)Math.floor(height);
		this.dx = 0;
		this.dy = 0;
		this.speed = 2;
	}
	
	public Entity(World world) {
		super(Enemy.getSprite());
		this.world = world;
		this.x = (int)Math.floor(width);
		this.y = (int)Math.floor(height);
		this.dx = 0;
		this.dy = 0;
		this.speed = 2;
	}

	@Override
	public void gameObjectCollisionOccurred(List<GameObject> collidedGameObjects) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tileCollisionOccurred(List<CollidedTile> collidedTiles) {
		PVector vector;

		for (CollidedTile ct : collidedTiles) {
			if (ct.getTile() instanceof WallTile) {
				System.out.println(ct.getCollisionSide());
				if (CollisionSide.TOP.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setY(vector.y - (getHeight() + 1));
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}

				if (CollisionSide.BOTTOM.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setY(vector.y + getHeight() + (64 - getHeight()));
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}

				if (CollisionSide.RIGHT.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setX(vector.x + getWidth() + (64 - getWidth()));
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}

				if (CollisionSide.LEFT.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setX(vector.x - (getWidth() + 1));
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}

				if (CollisionSide.INSIDE.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setX(100);
						setY(100);
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

}
