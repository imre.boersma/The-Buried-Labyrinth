package nl.imre.tbl.entities;

import java.util.List;

import nl.han.ica.oopg.collision.CollidedTile;
import nl.han.ica.oopg.collision.CollisionSide;
import nl.han.ica.oopg.collision.ICollidableWithGameObjects;
import nl.han.ica.oopg.collision.ICollidableWithTiles;
import nl.han.ica.oopg.exceptions.TileNotFoundException;
import nl.han.ica.oopg.objects.GameObject;
import nl.han.ica.oopg.objects.Sprite;
import nl.han.ica.oopg.objects.SpriteObject;
import nl.imre.tbl.Sprites;
import nl.imre.tbl.World;
import nl.imre.tbl.tiles.WallTile;
import processing.core.PVector;

public class Player extends SpriteObject implements ICollidableWithTiles, ICollidableWithGameObjects {
	public static Sprite sprite = new Sprite(Sprites.COIN.getUrl());
	private static int x;

	private static int y;
	/**
	 * @return the x
	 */
	public static float getPlayerX() {
		return x;
	}
	/**
	 * @return the y
	 */
	public static float getPlayerY() {
		return y;
	}

	private final World world;

	private double dx, dy, speed;

	private boolean left;
	private boolean right;
	private boolean up;
	private boolean down;

	public Player(World world) {
		super(sprite);
		this.world = world;
		Player.x = (int) Math.floor(width);
		Player.y = (int) Math.floor(height);
		this.dx = 0;
		this.dy = 0;
		this.speed = 2;
	}

	@Override
	public void gameObjectCollisionOccurred(List<GameObject> collidedGameObjects) {
		// TODO Auto-generated method stub
		
	}

	public void setDown(boolean down) {
		this.down = down;
	}

	public void setLeft(boolean left) {
		this.left = left;
	}

	public void setRight(boolean right) {
		this.right = right;
	}


	public void setUp(boolean up) {
		this.up = up;
	}

	@Override
	public void tileCollisionOccurred(List<CollidedTile> collidedTiles) {
		PVector vector;

		for (CollidedTile ct : collidedTiles) {
			if (ct.getTile() instanceof WallTile) {
				if (CollisionSide.TOP.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setY(vector.y - (getHeight() + 1));
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}

				if (CollisionSide.BOTTOM.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setY(vector.y + getHeight() + (64 - getHeight()));
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}

				if (CollisionSide.RIGHT.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setX(vector.x + getWidth() + (64 - getWidth()));
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}

				if (CollisionSide.LEFT.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setX(vector.x - (getWidth() + 1));
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}

				if (CollisionSide.INSIDE.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setX(100);
						setY(100);
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		}		
	}

	@Override
	public void update() {
		if (left) {
			dx = -speed;
		}
		if (right) {
			dx = speed;
		}
		if (up) {
			dy = -speed;
		}
		if (down) {
			dy = speed;
		}
		x = (int) this.getX();
		y = (int) this.getY();
		this.setX(x += dx);
		this.setY(y += dy);

		dx = 0;
		dy = 0;
	}
}
