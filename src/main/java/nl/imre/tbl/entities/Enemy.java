/**
 * 
 */
package nl.imre.tbl.entities;

import java.util.List;

import nl.han.ica.oopg.collision.CollidedTile;
import nl.han.ica.oopg.collision.CollisionSide;
import nl.han.ica.oopg.collision.ICollidableWithGameObjects;
import nl.han.ica.oopg.collision.ICollidableWithTiles;
import nl.han.ica.oopg.exceptions.TileNotFoundException;
import nl.han.ica.oopg.objects.GameObject;
import nl.han.ica.oopg.objects.Sprite;
import nl.han.ica.oopg.objects.SpriteObject;
import nl.imre.tbl.Sprites;
import nl.imre.tbl.World;
import nl.imre.tbl.tiles.WallTile;
import processing.core.PVector;

/**
 * @author Imre Boersma
 *
 */
public class Enemy extends SpriteObject implements ICollidableWithGameObjects, ICollidableWithTiles {
	public static Sprite sprite = new Sprite(Sprites.BIGDEMON.getUrl());
	/**
	 * @return the sprite
	 */
	public static Sprite getSprite() {
		return sprite;
	}

	private final World world;
	private float x;
	private float y;
	private float dx;
	private float dy;
	private float speed;

	private Boolean collided;
	
	public Enemy(World world) {
		super(sprite);
		this.world = world;
		this.x = (int) Math.floor(width);
		this.y = (int) Math.floor(height);
		this.dx = 0;
		this.dy = 0;
		this.speed = 1;
		this.collided = false;
	}
	@Override
	public void gameObjectCollisionOccurred(List<GameObject> collidedGameObjects) {

	}

	@Override
	public void tileCollisionOccurred(List<CollidedTile> collidedTiles) {
		PVector vector;

		for (CollidedTile ct : collidedTiles) {
			if (ct.getTile() instanceof WallTile) {
				this.collided = true;
				if (CollisionSide.TOP.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setY(vector.y - (getHeight() + 1));
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}

				if (CollisionSide.BOTTOM.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setY(vector.y + getHeight() + (64 - getHeight()));
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}

				if (CollisionSide.RIGHT.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setX(vector.x + getWidth() + (64 - getWidth()));
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}

				if (CollisionSide.LEFT.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setX(vector.x - (getWidth() + 1));
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}

				if (CollisionSide.INSIDE.equals(ct.getCollisionSide())) {
					try {
						vector = world.getTileMap().getTilePixelLocation(ct.getTile());
						setX(100);
						setY(100);
					} catch (TileNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
			this.collided = false;
		}		
	}

	@Override
	public void update() {

    float diffX = Player.getPlayerX() - x;
    float diffY = Player.getPlayerY() - y;

    float angle = (float) Math.atan2(diffY, diffX);

    if(!collided) {
      setX(x += speed * Math.cos(angle)); 
      setY(y += speed * Math.sin(angle));
    }
	}
}
