package nl.imre.tbl;

/**
 * @author imreb
 * ENums for all sprites used in the game.
 */

public enum Sprites {
	
	// Heroes
	ELF("src/media/entity/heroes/elf/elf_animated.gif"),
	KNIGHT("src/media/entity/heroes/knight/knight_animated.gif"),
	LIZARD("src/media/entity/heroes/lizard/lizard_animated.gif"),
	WIZZARD("src/media/entity/heroes/wizzard/wizzard_animated.gif"),
	
	// Demons
	BIGDEMON("src/media/entity/demons/big_demon/big_demon_animated.gif"),
	NECROMANCER("src/media/entity/demons/necromancer/necromancer_animated.gif"),
	
	// Undead
	BIGZOMBIE("src/media/entity/undead/big_zombie/big_zombie_animated.gif"),
	TINYZOMBIE("src/media/entity/undead/tiny_zombie/tiny_zombie_animated.gif"),
	
	// Entities
	COIN("src/media/item/coin/coin_animated.gif"),
	
	// Floors
	FLOOR1("src/media/structure/floor/floor_1.png"),
	FLOOR2("src/media/structure/floor/floor_2.png"),
	FLOOR3("src/media/structure/floor/floor_3.png"),
	FLOOR4("src/media/structure/floor/floor_4.png"),
	FLOOR5("src/media/structure/floor/floor_5.png"),
	FLOOR6("src/media/structure/floor/floor_6.png"),
	FLOOR7("src/media/structure/floor/floor_7.png"),
	FLOOR8("src/media/structure/floor/floor_8.png"),
	TRAP("src/media/structure/floor/floor_spike_animated.gif"),

	//Walls
	WALL("src/media/structure/wall/wall_mid.png"),
	WALLGOO("src/media/structure/wall/wall_goo.png"),
	WALLHOLE1("src/media/structure/wall/wall_hole_1.png"),
	WALLHOLE2("src/media/structure/wall/wall_hole_2.png"),
	WALLBANNERYELLOW("src/media/structure/wall/banner/wall_banner_yellow.png"),
	WALLBANNERRED("src/media/structure/wall/banner/wall_banner_red.png"),
	WALLBANNERGREEN("src/media/structure/wall/banner/wall_banner_green.png"),
	WALLBANNERBLUE("src/media/structure/wall/banner/wall_banner_blue.png"),

	//Doors
	DOORCLOSED("src/media/structure/door/doors_leaf_closed.png"),
	DOOROPEN("src/media/structure/door/doors_leaf_open.png");
	
	protected String url;
	
	Sprites(String url) {
		this.url = url;
	}
	
	public String getUrl() {
		return this.url;
	}
}
